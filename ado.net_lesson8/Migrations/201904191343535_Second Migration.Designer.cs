// <auto-generated />
namespace ado.net_lesson8.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class SecondMigration : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(SecondMigration));
        
        string IMigrationMetadata.Id
        {
            get { return "201904191343535_Second Migration"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
