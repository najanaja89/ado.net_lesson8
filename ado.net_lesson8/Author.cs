﻿using System.Collections.Generic;

namespace ado.net_lesson8
{
    public class Author:Entity
    {
        public string Name { get; set; }
        public ICollection<Book> Books { get; set; }
    }
}