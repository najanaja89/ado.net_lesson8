﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ado.net_lesson8
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var context = new DataContex())
            {
                #region Create DataBase and Table
                //var author = new Author
                //{
                //    Name = "D. Simmons"
                //};

                //var book = new Book
                //{
                //    Name = "Hyperion",
                //    Price = 100,
                //    AuthorId = author.Id

                //};

                //context.Authors.Add(author);
                //context.Books.Add(book);
                //context.SaveChanges(); 
                #endregion

                //var books = context.Books.ToList();
                //var author = books.FirstOrDefault().Author;
                var author = context.Authors.FirstOrDefault();
                context.Entry(author).Collection("Books").Load();
            }
        }
    }
}
